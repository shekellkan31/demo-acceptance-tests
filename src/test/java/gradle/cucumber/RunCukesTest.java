package gradle.cucumber;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(plugin = { "pretty", "html:build/reports/cucumber","json:build/test-results/cucumber-report.json"}, monochrome = true)
public class RunCukesTest {
  
  //This test is intentionally blank
  
  //See: 
  //  * src/test/resources for Gherkin Feature files
  
}