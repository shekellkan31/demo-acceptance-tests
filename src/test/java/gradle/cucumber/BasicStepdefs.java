package gradle.cucumber;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import io.cucumber.java.en.Then;
//import io.cucumber.api.PendingException;
import static org.junit.Assert.*;

public class BasicStepdefs {

    @Given("A failing scenario")
    public void a_failing_scenario() {
        //throw new io.cucumber.java.PendingException();
    }

    @When("I run a failing step")
    public void i_run_a_failing_step() {
        //throw new io.cucumber.java.PendingException();
    }

    @Then("I got a failing step")
    public void i_got_a_failing_step() {
        //throw new io.cucumber.java.PendingException();
    }
}